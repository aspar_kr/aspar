--- 
title: "The _Aspergillus fumigatus_ azole knowledge repository"
author: "Sibbe Bakker"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
output: bookdown::gitbook
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
github-repo: rstudio/bookdown-demo
description: "This book is the main documentation for the ASPAR_KR project"
---

```{r message=FALSE, warning=TRUE, include=FALSE}
library(plantuml)

```


```{r include=FALSE}
source("script.R")
# automatically create a bib database for R packages
knitr::write_bib(c(
  .packages(), 
  'bookdown', 
  'knitr', 
  'rmarkdown'),
  'packages.bib')
```

<!-- # Acknowledgements -->

<!-- This book was written with  -->

